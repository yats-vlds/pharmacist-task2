import React, {useEffect, useState} from "react";
import "./App.css"
import clock from "./assets/icons/time.svg"
import question from "./assets/icons/question.svg"
import answerA from "./assets/icons/AnswerA.svg"
import answer_A_After from "./assets/icons/Answer-A-After.svg"
import answer_A_Mistake from "./assets/icons/Answer-A-Mistake.svg"
import answer_B_After from "./assets/icons/Answer-B-After.svg"
import answer_B_Mistake from "./assets/icons/Answer-B-Mistake.svg"
import answer_C_After from "./assets/icons/Answer-C-After.svg"
import answer_C_Mistake from "./assets/icons/Answer-C-Mistake.svg"
import answerB from "./assets/icons/AnswerB.svg"
import answerC from "./assets/icons/AnswerC.svg"
import grassLeft from "./assets/icons/grassLeft.svg"
import grassRight from "./assets/icons/grassRight.svg"
import table from "./assets/icons/teamTable.svg"
import yogi1 from "./assets/icons/yogi1.svg"
import yogi2 from "./assets/icons/yogi2.svg"
import yogi3 from "./assets/icons/yogi3.svg"
import yogi4 from "./assets/icons/yogi4.svg"
import BarbedWire from "./assets/icons/BarbedWire.svg"
import logo from "./assets/icons/baksokolin-logo.svg"
import Flowerpot from "./assets/icons/Flowerpot.svg"

function App() {
    const TIMER = {
        PAUSED: "PAUSED",
        TICKING: "TICKING"
    };
    const [text, setText] = useState(60);
    const [current, setCurrent] = useState(null);
    const [timerState, setTimerState] = useState(TIMER.PAUSED);
    const [check, setCheck] = useState(false)
    const [checkEl, setCheckEl] = useState(null)
    const [countQuestion, setCountQuestion] = useState(0)
    const [showAnswer, setShowAnswer] = useState(false)
    const [team1Count, setTeam1Count] = useState(0)
    const [team2Count, setTeam2Count] = useState(0)
    const [team3Count, setTeam3Count] = useState(0)
    const [team4Count, setTeam4Count] = useState(0)


    function isTicking() {
        return timerState === TIMER.TICKING;
    }

    function pause() {
        setTimerState(TIMER.PAUSED);
    }

    function handleStart() {
        if (isTicking()) {
            return;
        }

        if (!current) {
            setCurrent(Number(text));
        }
        setShowAnswer(true)

        setTimerState(TIMER.TICKING);
    }

    function handlePause() {
        pause();
    }

    useEffect(() => {
        let intervalId;

        function reset() {
            pause();
            setCurrent("0");
        }

        function pauseTimer() {
            clearInterval(intervalId);
        }

        function tick() {
            setCurrent((current) => {
                if (current === 0) {
                    reset();
                }

                return current - 1;
            });
        }

        switch (timerState) {
            case TIMER.PAUSED:
                break;
            case TIMER.TICKING:
                intervalId = setInterval(tick, 1000);
                break;
            default:
                break;
        }

        return pauseTimer;
    }, [timerState]);


    let textContent = [
        {
            question: "За какое минимальное время человек способен запомнить последователь-ность из 52 перетасованих карт и точно воспроизвести ее?",
            answer1: "12.7 cек",
            answer2: "10 мин",
            answer3: "1.5 мин",
            true: "answer2"
        },
        {
            question: "Привіт молодий чоловіче?2",
            answer1: "Хай2",
            answer2: "хАЮ2",
            answer3: "Хелоу2",
            true: "answer3"
        },
        {
            question: "Привіт молодий чоловіче?3",
            answer1: "Хай3",
            answer2: "хАЮ3",
        },
        {
            question: "Привіт молодий чоловіче?4",
            answer1: "Хай4",
            answer2: "хАЮ4",
            answer3: "Хелоу4"
        },
        {
            question: "Дякую за увагу"
        }
    ]

    let countQuestionHandler = () => {
        setCountQuestion(countQuestion < textContent.length - 1 ? countQuestion + 1 : countQuestion)
        setCurrent(null)
        setCheckEl(null)
        setCheck(false)
        setShowAnswer(false)
    }

    let checkAnswerElHandler = (answer) => {
        setCheckEl(answer)
        setShowAnswer(true)
        handlePause()
    }

    console.log(`Первая команда очков ${team1Count}`)
    console.log(`Вторая команда очков ${team2Count}`)
    console.log(`Третья команда очков ${team3Count}`)
    console.log(`Четвертая команда очков ${team4Count}`)
    return (
        <div className="farmac-task2">
            <div className="container-fluid">
                <div className="row justify-content-center">
                    <div className="col">
                        <div>
                            <img src={clock} alt="clock" className="w-50 h-75 clock__img"/>
                            <h1 className="display-3 text-clock">{current === null ? 60 : current}</h1>
                            <button onClick={handleStart} disabled={isTicking()} className="start">
                                Start
                            </button>
                            <button onClick={handlePause} disabled={!isTicking()} className="pause">
                                Pause
                            </button>
                            <button className="reset" onClick={() => setCurrent(null)}>RESET</button>
                        </div>
                        <div>
                            <img src={question} alt="table-question" className={`w-50 h-75 tableQuestion__img`}/>
                            <h1 className="display-6 text-question"
                                onClick={() => countQuestionHandler()}>{textContent[countQuestion].question}</h1>
                        </div>
                        <div
                            className={!showAnswer ? "answer0" : "answer"}>
                            {textContent[countQuestion].answer1 && showAnswer ?
                                <>
                                    <img src={checkEl === 'answer1' ? textContent[countQuestion].true === 'answer1' ? answer_A_After : answer_A_Mistake : answerA} alt="answer A"
                                         className="answer__img"
                                         onClick={() => checkAnswerElHandler('answer1')}/>
                                    <h5 className="text-answerA"
                                        onClick={() => checkAnswerElHandler('answer1')}
                                    >{textContent[countQuestion].answer1}</h5>
                                </> : ""}
                            {textContent[countQuestion].answer2 && showAnswer ?
                                <>
                                    <img src={checkEl === 'answer2' ? textContent[countQuestion].true === 'answer2' ? answer_B_After : answer_B_Mistake : answerB} alt="answer B"
                                         className="answer__img"
                                         onClick={() => checkAnswerElHandler('answer2')}
                                    />
                                    <h5 className="text-answerB"
                                        onClick={() => checkAnswerElHandler('answer2')}
                                    >{textContent[countQuestion].answer2}</h5>
                                </> : ""}
                            {textContent[countQuestion].answer3 && showAnswer ?
                                <>
                                    <img src={checkEl === 'answer3' ? textContent[countQuestion].true === 'answer3' ? answer_C_After : answer_C_Mistake : answerC} alt="answer C"
                                         className="answer__img"
                                         onClick={() => checkAnswerElHandler('answer3')}/>
                                    <h5 className="text-answerC"
                                        onClick={() => checkAnswerElHandler('answer3')}
                                    >{textContent[countQuestion].answer3}</h5>
                                </> : ""}
                        </div>
                    </div>
                </div>
                <div className="content"/>
                <img src={BarbedWire} className="barbed-wire"/>
                <div className="row">
                    <div className="col">
                        <img src={grassLeft} className="h-75 gressLeft__img"/>
                        <img src={Flowerpot} className="flowerpot__img w-25 h-25"/>
                        <img src={table} className="table__img" onClick={() => alert("TABLE")}/>
                        <img src={yogi1}
                             className="yogi__img"
                             style={{animation: `yogiImg${team1Count} 4s linear infinite`}}
                             onClick={() => 9 > team1Count ? setTeam1Count(team1Count + 1) : team1Count}
                        />
                    </div>
                    {/* <div className="col">
                        <img src={table} className="table__img2"/>
                        <img src={yogi2} className="yogi__img2"
                             style={{animation: `yogiImg${team2Count} 4s linear infinite`}}
                             onClick={() => 9 > team2Count ? setTeam2Count(team2Count + 1) : team2Count}
                        />
                    </div> */}

                    <div className="col">
                        <img src={table} className="table__img3"/>
                        <img src={yogi3} className="yogi__img3"
                             style={{animation: `yogiImg${team3Count} 4s linear infinite`}}
                             onClick={() => 9 > team3Count ? setTeam3Count(team3Count + 1) : team3Count}
                        />
                    </div>
                    <div className="col">
                        <img src={table} className="table__img4"/>
                        <img src={yogi4} className="yogi__img4"
                             style={{animation: `yogiImg${team4Count} 4s linear infinite`}}
                             onClick={() => 9 > team4Count ? setTeam4Count(team4Count + 1) : team4Count}
                        />
                        <img src={grassRight} className="h-75 gressRight__img"/>
                        <img src={logo} className="logo__img"/>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default App;
